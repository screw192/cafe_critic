const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Place = require("./models/Place");
const Review = require("./models/Review");
const AdditionalPhotos = require("./models/AdditionalPhotos");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) { 
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [admin, userYoba, userAnon, userChan] = await User.create({
    email: "user1@test",
    password: "test",
    token: nanoid(),
	  role: "admin",
    displayName: "Abu Makaka"
  }, {
    email: "user2@test",
    password: "test",
    token: nanoid(),
    displayName: "Yoba YellowKolobok",
  }, {
    email: "user3@test",
    password: "test",
    token: nanoid(),
    displayName: "Anonim Legion"
  }, {
    email: "user4@test",
    password: "test",
    token: nanoid(),
    displayName: "Tyanocka"
  });

  const [bar1, cafe1, rest1] = await Place.create({
	  title: "Some Bar",
	  description: "Some bar in city center on 23rd floor",
	  mainPhoto: "fixtures/bar_main.png",
	  user: admin,
  }, {
	  title: "Some Cafe",
	  description: "Some cafe in city center on crossing of Pupa and Lupa streets",
	  mainPhoto: "fixtures/cafe_main.png",
	  user: userYoba,
  }, {
	  title: "Some Restaurant",
	  description: "Some restaurant in city center on Yoba boulevard 359",
	  mainPhoto: "fixtures/rest_main.png",
	  user: userChan,
  });

  await Review.create({
	  reviewText: "Очень хороший бар, понравилось. Топ за свои деньги",
	  drinksFoodRating: 5,
	  serviceRating: 5,
	  interiorRating: 5,
	  user: admin,
	  place: bar1,
  }, {
	  reviewText: "Классный бар, бармен краш. Теперь каждые выходные буду напиваться в говно там.",
	  drinksFoodRating: 5,
	  serviceRating: 5,
	  interiorRating: 4,
	  user: userChan,
	  place: bar1,
  }, {
	  reviewText: "Интерьер конечно классный и заведение в целом, но бармен бадяжит бухло.",
	  drinksFoodRating: 3,
	  serviceRating: 2,
	  interiorRating: 5,
	  user: userAnon,
	  place: bar1,
  }, {
	  reviewText: "Вчера пришли выпить, много разного пивандрия. Рекомендую.",
	  drinksFoodRating: 5,
	  serviceRating: 4,
	  interiorRating: 4,
	  user: userYoba,
	  place: bar1,
  }, {
	  reviewText: "Очень вкусная еда, и цены вполне адекватные.",
	  drinksFoodRating: 5,
	  serviceRating: 5,
	  interiorRating: 3,
	  user: userChan,
	  place: cafe1,
  }, {
	  reviewText: "Тут самый вкусный чечевичный суп! Если скучаете по Турции, рекомендую!",
	  drinksFoodRating: 5,
	  serviceRating: 5,
	  interiorRating: 4,
	  user: admin,
	  place: cafe1,
  }, {
	  reviewText: "Почти всегда тихо и спокойно. Нет орущей музыки.",
	  drinksFoodRating: 4,
	  serviceRating: 4,
	  interiorRating: 5,
	  user: userYoba,
	  place: cafe1,
  }, {
	  reviewText: "Дорого и говно, не реконмендую. Не думайте что я нищеброд, я зарабатываю 300К в наносекунду.",
	  drinksFoodRating: 1,
	  serviceRating: 1,
	  interiorRating: 1,
	  user: userAnon,
	  place: rest1,
  }, {
	  reviewText: "Простот отвал башки! 100/10 господи! 100/10!!! GOTY однозначно!",
	  drinksFoodRating: 5,
	  serviceRating: 5,
	  interiorRating: 5,
	  user: userChan,
	  place: rest1,
  });

  await AdditionalPhotos.create({
	  image: "fixtures/bar1.png",
	  user: userYoba,
	  place: bar1,
  }, {
	  image: "fixtures/bar2.png",
	  user: userChan,
	  place: bar1,
  }, {
	  image: "fixtures/cafe1.png",
	  user: userAnon,
	  place: cafe1,
  }, {
	  image: "fixtures/cafe2.png",
	  user: userYoba,
	  place: cafe1,
  }, {
	  image: "fixtures/rest1.png",
	  user: userChan,
	  place: rest1,
  }, {
	  image: "fixtures/rest2.png",
	  user: userChan,
	  place: rest1,
  });

  await mongoose.connection.close();
};

run().catch(console.error);
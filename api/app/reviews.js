const express = require("express");
const auth = require("../middleware/auth");
const Review = require("../models/Review");
const mongoose = require("mongoose");

const router = express.Router();

router.post("/", auth, async (req, res) => {
	try {
		const reviewData = {
			user: req.user._id,
			place: req.body.place,
			reviewText: req.body.reviewText,
			drinksFoodRating: req.body.drinksFoodRating,
			serviceRating: req.body.serviceRating,
			interiorRating: req.body.interiorRating,
		};

		const review = await new Review(reviewData).save(); // тупое говно не хотело отрабатывать .save() если писать в две отдельные строки. При том что в places.js работает

		res.send(review);
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;
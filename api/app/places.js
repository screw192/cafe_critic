const express = require("express");
const auth = require("../middleware/auth");
const upload = require("../multer").places;
const Place = require("../models/Place");
const Review = require("../models/Review"); // import needed for population virtual data
const AdditionalPhotos = require("../models/AdditionalPhotos"); // import needed for population virtual data

const router = express.Router();

router.get("/all", async (req, res) => {
	try {
		const placesData = await Place
			.find({})
			.populate("reviews", ["drinksFoodRating", "serviceRating", "interiorRating"])
			.populate("photos", "_id");

		res.send(placesData);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.get("/:id", async (req, res) => {
	try {
		const placeData = await Place
			.findOne({_id: req.params.id})
			.populate({path: "reviews", populate: {path: "user", select: "displayName"}})
			.populate("photos");

		res.send(placeData);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.post("/", auth, upload.single("mainPhoto"), async (req, res) => {
	try {
		if (!req.body.agreement) {
			return res.status(403).send({message: "You need to accept user agreement!"});
		}

		const placeData = {
			user: req.user._id,
			title: req.body.title,
			description: req.body.description,
		};
		placeData.mainPhoto = req.file.filename;

		const place = new Place(placeData);
		await place.save();

		res.send(place);
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;
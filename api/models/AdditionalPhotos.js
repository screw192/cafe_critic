const mongoose = require('mongoose');

const AdditionalPhotosSchema = new mongoose.Schema({
	image: {
		type: String,
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	place: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Place',
		required: true,
	}
});

const AdditionalPhotos = mongoose.model('AdditionalPhotos', AdditionalPhotosSchema);
module.exports = AdditionalPhotos;
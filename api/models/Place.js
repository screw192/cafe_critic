const mongoose = require('mongoose');

const PlaceSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	mainPhoto: {
		type: String,
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	}
}, {toJSON: {virtuals: true}});

PlaceSchema.virtual('reviews', {
	ref: 'Review',
	localField: "_id",
	foreignField: 'place',
	justOne: false
});

PlaceSchema.virtual('photos', {
	ref: 'AdditionalPhotos',
	localField: "_id",
	foreignField: 'place',
	justOne: false
});

const Place = mongoose.model('Place', PlaceSchema);
module.exports = Place;
const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
	reviewText: {
		type: String,
		required: true,
	},
	drinksFoodRating: {
		type: Number,
		enum: [1, 2, 3, 4, 5],
		required: true,
	},
	serviceRating: {
		type: Number,
		enum: [1, 2, 3, 4, 5],
		required: true,
	},
	interiorRating: {
		type: Number,
		enum: [1, 2, 3, 4, 5],
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	place: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Place',
		required: true,
	}
});

const Review = mongoose.model('Review', ReviewSchema);
module.exports = Review;
const path = require("path");
const rootPath = __dirname;

const env = process.env.NODE_ENV;

let databaseUrl = "mongodb://localhost/cafe_critic"; // change to needed
let port = 8000;

switch (env) {
	case "test":
		databaseUrl = "mongodb://localhost/cafe_critic_test"; // change to needed for test
		port = 8010;
		break;
	default:
		break;
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public"),
	port,
  db: {
    url: databaseUrl,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: process.env.FACEBOOK_APP_ID,
    appSecret: process.env.FACEBOOK_APP_SECRET,
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID
  }
};
const { I } = inject();

Given('I am on registration page', () => {
	I.amOnPage("/register");
	I.wait(4);
});

Given('I am on login page', () => {
	I.amOnPage("/login");
	I.wait(4);
});

When('I fill input fields with:', (table) => {
  for (const id in table.rows) {
  	if (id < 1) {
  		continue;
	  }

  	const cells = table.rows[id].cells;
  	const field = cells[0].value;
  	const value = cells[1].value;

  	I.fillField(field, value);
  }
  I.wait(2);
});

When('press {string} button to log in', (label) => {
  I.click(`//button//*[contains(text(),"${label}")]/..`);
	I.wait(2);
});

When('press {string} button to open user menu', (label) => {
	I.click(`//button[span[contains(text(), '${label}')]]`);
	I.wait(0.5);
});

When('press {string} menu button', (label) => {
	I.clickLink(`//a[@href='/add_place'][contains(text(), '${label}')]`);
	I.wait(0.5);
});

When('press {string} button to add place', (label) => {
	I.click(`//button[span[contains(text(), '${label}')]]`);
	I.wait(0.5);
});

Then('I click {string} link', (placeName) => {
	I.clickLink(`//a[div[contains(@title, '${placeName}')]]`);
	I.wait(2);
});

Then('I see {string}', (text) => {
  I.see(text);
});
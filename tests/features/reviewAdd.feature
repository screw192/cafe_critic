Feature: Add review

  @addreview
  Scenario: User opens page with some place, and leaves review.
    Given I am on login page
    When I fill input fields with:
      | field    | value      |
      | email    | user2@test |
      | password | test       |
    And press "Login" button
    Then I see "YOBA YELLOWKOLOBOK"
    Then I click "Some Restaurant" link
    Then I fill input fields with:
      | field | value |
      | reviewText | Good place. OCHE advice! |
      | drinksFoodRating | 5 |
      | serviceRating | 5 |
      | interiorRating | 5 |
    And press "Add review" button
    Then I see "Good place. OCHE advice!"
    Then I see "Overall: 3.7"
    Then I see "Quality of food: 3.7"
    Then I see "Service quality: 3.7"
    Then I see "Interior: 3.7"

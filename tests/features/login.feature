Feature: User login

  @login
  Scenario: User login
    Given I am on login page
    When I fill input fields with:
      | field    | value      |
      | email    | user2@test |
      | password | test       |
    And press "Login" button
    Then I see "YOBA YELLOWKOLOBOK"
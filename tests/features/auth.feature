Feature: User authentication

  @registration
  Scenario: User registration
    Given I am on registration page
    When I fill input fields with:
      | field       | value                |
      | email       | rickCucumber@test.rm |
      | password    | SomeKindOfPassword   |
      | displayName | Rick Sanchez         |
    And press "Register" button
    Then I see "Rick Sanchez"
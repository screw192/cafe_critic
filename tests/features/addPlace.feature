Feature: Add place

  @addplace
  Scenario: User adds new place.
    Given I am on login page
    When I fill input fields with:
      | field    | value      |
      | email    | user2@test |
      | password | test       |
    And press "Login" button to log in
    Then I see "YOBA YELLOWKOLOBOK"
    Then press "Yoba YellowKolobok" button to open user menu
    Then press "Add new place" menu button
    Then I fill input fields with:
      | field       | value                                            |
      | title       | Some cateen                                      |
      | description | Cheap canteen somewhere in the middle of nowhere |
    Then I attach place file with:
      | field | value |
      |  |  |
    And press "Add place" button to add place
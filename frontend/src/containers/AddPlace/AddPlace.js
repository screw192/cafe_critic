import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Typography} from "@material-ui/core";
import {Helmet} from "react-helmet";

import AddPlaceForm from "../../components/AddPlaceForm/AddPlaceForm";
import {uploadPlaceRequest} from "../../store/actions/placesActions";


const AddPlace = () => {
	const dispatch = useDispatch();
	const loading = useSelector(state => state.places.addPlaceLoading);
	const error = useSelector(state => state.places.addPlaceError);

	const uploadPhotoFormSubmit = placeData => {
		dispatch(uploadPlaceRequest(placeData));
	};

	return (
		<Container maxWidth="lg">
			<Helmet>
				<title>Add place</title>
			</Helmet>
			<Typography variant="h5" gutterBottom>
				Add your photo
			</Typography>
			<AddPlaceForm
				onSubmit={uploadPhotoFormSubmit}
				loading={loading}
				error={error}
			/>
		</Container>
	);
};

export default AddPlace;
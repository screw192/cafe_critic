import React, {useEffect} from 'react';
import {Helmet} from "react-helmet";
import {Box, Container, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPlacesRequest} from "../../store/actions/placesActions";
import PlacesCard from "./PlacesCard";
import Preloader from "../../components/UI/Preloader/Preloader";

const AllPlaces = () => {
	const dispatch = useDispatch();
	const placesLoading = useSelector(state => state.places.placesLoading);
	const places = useSelector(state => state.places.places);

	useEffect(() => {
		dispatch(fetchPlacesRequest());
	}, [dispatch]);

	const placesCards = places.map(place => {
		return (<PlacesCard key={place._id} id={place._id} placeData={place} />)
	});

	return (
		<Container component="section" maxWidth="lg">
			<Helmet>
				<title>All places</title>
			</Helmet>
			<Typography variant="h3" align="left" gutterBottom>All places</Typography>
			<Grid container component={Box} p={1} spacing={3}>
				{!placesLoading ? placesCards : <Preloader/>}
			</Grid>
		</Container>
		);
};

export default AllPlaces;
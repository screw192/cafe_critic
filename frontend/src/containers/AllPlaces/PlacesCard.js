import React from 'react';
import {Card, CardActionArea, CardContent, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 140,
	},
});

const PlacesCard = ({id, placeData}) => {
	const classes = useStyles();

	const averageRatings = placeData.reviews.reduce((accumulator, item) => {
		return accumulator + (item.drinksFoodRating + item.serviceRating + item.interiorRating)/3;
	}, 0);

	const overallRating = Math.round((averageRatings / placeData.reviews.length) * 10) / 10;

	return (
		<Grid item xs={4}>
			<Card className={classes.root}>
				<CardActionArea component={Link} to={`/place/${id}`}>
					<CardMedia
						className={classes.media}
						image={`${apiURL}/${placeData.mainPhoto}`}
						title={placeData.title}
					/>
					<CardContent>
						<Typography gutterBottom variant="h5" component="h2">
							{placeData.title}
						</Typography>
					</CardContent>
				</CardActionArea>
				<CardContent>
					<Typography>Rating here (stars)</Typography>
					<Typography>({overallRating}, {placeData.reviews.length} reviews)</Typography>
					<Typography>{placeData.photos.length} photos</Typography>
				</CardContent>
			</Card>
		</Grid>
	);
};

export default PlacesCard;
import React from 'react';
import {CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const useStyles = makeStyles({
	media: {
		height: 300,
	},
	galleryBlock: {
		margin: "20px 0",
		padding: "10px 0",
		borderTop: "1px solid #cccccc",
		borderBottom: "1px solid #cccccc"
	},
	galleryImage: {
		marginRight: "10px"
	},
	ratingsBlock: {
		paddingBottom: "10px",
		borderBottom: "1px solid #cccccc"
	}
});

const PlaceDetailsInfoBlock = ({placeData}) => {
	const classes = useStyles();

	let placePhotos = null;
	if (placeData.photos) {
		placePhotos = placeData.photos.map(photoData => {
			return (
				<img
					key={photoData._id}
					className={classes.galleryImage}
					src={`${apiURL}/${photoData.image}`}
					height="100px" width="auto"
					alt={placeData.title}
				/>
			)
		})
	}

	let placeRatings = null;
	if (placeData.reviews) {
		let overall = 0;
		let food = 0;
		let service = 0;
		let interior = 0;

		placeData.reviews.forEach(review => {
			food += review.drinksFoodRating;
			service += review.serviceRating;
			interior += review.interiorRating;
		});

		overall = Math.round(((food + service + interior) / (3 * placeData.reviews.length)) * 10) / 10;
		food = Math.round((food / placeData.reviews.length) * 10) / 10;
		service = Math.round((service / placeData.reviews.length) * 10) / 10;
		interior = Math.round((interior / placeData.reviews.length) * 10) / 10;

		placeRatings = (
			<>
				<Typography variant="body2">{`Overall: ${overall}`}</Typography>
				<Typography variant="body2">{`Quality of food: ${food}`}</Typography>
				<Typography variant="body2">{`Service quality: ${service}`}</Typography>
				<Typography variant="body2">{`Interior: ${interior}`}</Typography>
			</>
		);
	}

	return (
		<Grid item xs container direction="column">
			<Grid item xs container>
				<Grid item xs={7}>
					<Typography variant="h4" gutterBottom>{placeData.title}</Typography>
					<Typography variant="h6">{placeData.description}</Typography>
				</Grid>
				<Grid item xs={5}>
					<CardMedia
						className={classes.media}
						image={`${apiURL}/${placeData.mainPhoto}`}
					/>
				</Grid>
			</Grid>
			<Grid item xs className={classes.galleryBlock}>
				<Typography variant="h6">Gallery</Typography>
				{placeData.photos ?
					placePhotos :
					<Typography variant="body2">Sorry no additional photos were added yet</Typography>}
			</Grid>
			<Grid item xs className={classes.ratingsBlock}>
				<Typography variant="h6">Ratings</Typography>
				{placeData.reviews ?
					placeRatings :
					<Typography variant="body2">Sorry no reviews were added yet</Typography>}
			</Grid>
		</Grid>
	);
};

export default PlaceDetailsInfoBlock;
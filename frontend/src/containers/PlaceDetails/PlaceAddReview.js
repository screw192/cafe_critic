import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import AddReviewForm from "../../components/AddReviewForm/AddReviewForm";
import {useDispatch, useSelector} from "react-redux";
import {addReviewRequest} from "../../store/actions/reviewActions";

const useStyles = makeStyles({
	reviewFormBlock: {
		paddingTop: "10px",
		borderTop: "1px solid #cccccc"
	}
});

const PlaceAddReview = ({placeId}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const loading = useSelector(state => state.review.addReviewLoading);
	const error = useSelector(state => state.review.addReviewError);

	const addReviewFormSubmit = reviewData => {
		dispatch(addReviewRequest({reviewData, placeId}));
	};

	return (
		<>
			<Grid item xs>
				<Typography variant="h6" gutterBottom className={classes.reviewFormBlock}>
					Add review
				</Typography>
				<AddReviewForm
					placeId={placeId}
					onSubmit={addReviewFormSubmit}
					loading={loading}
					error={error}
				/>
			</Grid>
		</>
	);
};

export default PlaceAddReview;
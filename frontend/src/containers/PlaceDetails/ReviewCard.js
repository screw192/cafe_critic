import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles({
	reviewCard: {
		marginTop: "15px",
	}
});

const ReviewCard = ({reviewData}) => {
	const classes = useStyles();
	const foodReviewText = `Quality of food: ${reviewData.drinksFoodRating}`;
	const serviceReviewText = `Service quality: ${reviewData.drinksFoodRating}`;
	const interiorReviewText = `Interior: ${reviewData.serviceRating}`;

	return (
		<Grid item xs className={classes.reviewCard}>
			<Typography variant="body2"><b>{reviewData.user.displayName}</b> said:</Typography>
			<Typography variant="body2">{reviewData.reviewText}</Typography>
			<Typography variant="body2">{foodReviewText}</Typography>
			<Typography variant="body2">{serviceReviewText}</Typography>
			<Typography variant="body2">{interiorReviewText}</Typography>
		</Grid>
	);
};

export default ReviewCard;
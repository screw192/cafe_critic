import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPlaceDetailsRequest} from "../../store/actions/placesActions";
import {Helmet} from "react-helmet";
import {Box, Container, Grid} from "@material-ui/core";
import PlaceDetailsInfoBlock from "./PlaceDetailsInfoBlock";
import PlaceReviewsBlock from "./PlaceReviewsBlock";
import Preloader from "../../components/UI/Preloader/Preloader";
import PlaceAddReview from "./PlaceAddReview";

const PlaceDetails = props => {
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const placeDetailsLoading = useSelector(state => state.places.placeDetailsLoading);
	const placeData = useSelector(state => state.places.placeDetails);
	const placeId = props.match.params.id;

	useEffect(() => {
		dispatch(fetchPlaceDetailsRequest(placeId));
	}, [dispatch, placeId]);

	const placeInfoBlock = <PlaceDetailsInfoBlock placeData={placeData}/>
	const placeReviewsBlock = <PlaceReviewsBlock reviews={placeData.reviews}/>
	let placeAddReviewBlock = null;
	if (user) {
		placeAddReviewBlock = <PlaceAddReview placeId={placeId}/>;
	}

	return (
		<Container component="section" maxWidth="lg">
			<Helmet>
				<title>{placeData.title}</title>
			</Helmet>
			<Grid container direction="column" component={Box} p={1} spacing={3}>
				{!placeDetailsLoading ? (
					<>
						{placeInfoBlock}
						{placeReviewsBlock}
						{placeAddReviewBlock}
					</>
				) : (
					<Preloader/>
				)}
			</Grid>
		</Container>
	);
};

export default PlaceDetails;
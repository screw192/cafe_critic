import React from 'react';
import {Grid, Typography} from "@material-ui/core";
import ReviewCard from "./ReviewCard";

const PlaceReviewsBlock = ({reviews}) => {

	let placeReviews = null;
	if (reviews) {
		placeReviews = reviews.map(review => {
			return (<ReviewCard key={review._id} reviewData={review}/>);
		});
	}

	return (
		<>
			<Grid item xs container direction="column">
				<Typography variant="h6">Reviews</Typography>
				{placeReviews}
			</Grid>
		</>
	);
};

export default PlaceReviewsBlock;
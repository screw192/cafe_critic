const env = process.env.REACT_APP_ENV;

let exportUrl = "http://localhost:8000";
if (env === "test") {
	exportUrl = "http://localhost:8010";
}

export const apiURL = exportUrl;
export const googleClientId = process.env.REACT_APP_GOOGLE_CLIENT_ID;
export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;
import React from "react";
import {Route, Switch} from "react-router-dom";
import {Helmet} from "react-helmet";

import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AllPlaces from "./containers/AllPlaces/AllPlaces";
import PlaceDetails from "./containers/PlaceDetails/PlaceDetails";
import AddPlace from "./containers/AddPlace/AddPlace";

const App = () => {
  return (
    <Layout>
      <Helmet
        titleTemplate="Cafe critique - %s"
        defaultTitle="Cafe critique"
      />
      <Switch>
        <Route path="/" exact component={AllPlaces}/>
        <Route path="/place/:id" exact component={PlaceDetails}/>
        <Route path="/add_place" exact component={AddPlace}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
      </Switch>
    </Layout>
  );
};

export default App;

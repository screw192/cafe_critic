import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Avatar, Button, makeStyles, Menu, MenuItem} from "@material-ui/core";

import {logoutRequest} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";
import {Link} from "react-router-dom";


const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
	  marginLeft: "10px",
  }
}));

const UserMenu = ({user}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  let usersAvatar = null;
  if (user.avatar) {
  	usersAvatar = <Avatar
		  src={apiURL + "/" + user.avatar}
		  className={classes.avatar}
	  />
  } else {
  	usersAvatar = <Avatar className={classes.avatar}/>
  }

  return (
    <>
      <Button
        onClick={handleClick}
        color="inherit"
      >
	      {user.displayName}
	      {usersAvatar}
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem component={Link} to={"/add_place"}>Add new place</MenuItem>
        <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;
import React from 'react';
import {CircularProgress, makeStyles} from "@material-ui/core";


const useStyles = makeStyles({
  preloaderBox: {
    height: "300px",
    display: "flex",
	  margin: "0 auto",
    alignItems: "center",
	  transform: "scale(2)",
  },
});

const Preloader = () => {
  const classes = useStyles();

  return (
      <div className={classes.preloaderBox}>
        <CircularProgress />
      </div>
  );
};

export default Preloader;
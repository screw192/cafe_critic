import React, {useState} from 'react';
import {Button, FormControl, Grid, InputLabel, makeStyles, MenuItem, Select} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";

const useStyles = makeStyles({
	textField: {
		marginBottom: "20px"
	},
	formControl: {
		minWidth: 120,
	},
});

const AddReviewForm = ({placeId, onSubmit, loading, error}) => {
	const classes = useStyles();
	const [placeReview, setPlaceReview] = useState({
		reviewText: "",
		drinksFoodRating: 5,
		serviceRating: 5,
		interiorRating: 5,
	});

	const submitFormHandler = e => {
		e.preventDefault();
		onSubmit({...placeReview, place: placeId});
	};

	const placeReviewChangeHandler = e => {
		const value = e.target.value;

		setPlaceReview(prevState => ({
			...prevState,
			reviewText: value,
		}));
	};

	const handleRatingChange = (event) => {
		const name = event.target.name;
		const value = event.target.value;

		setPlaceReview(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const reviewValidator = () => {
		return placeReview.review === "";
	}

	return (
		<>
			<Grid
				container
				component="form"
				direction="column"
				onSubmit={submitFormHandler}
			>
				<Grid item xs className={classes.textField}>
					<FormElement
						required
						label="Your thoughts about this place:"
						type="text"
						name="reviewText"
						multiline
						rows={4}
						onChange={placeReviewChangeHandler}
						value={placeReview.reviewText}
						error={null}
					/>
				</Grid>
				<Grid item xs container justifyContent="space-between">
					<Grid item>
						<FormControl variant="outlined" size="small" className={classes.formControl}>
							<InputLabel id="foodQualitySelect">Food/Drinks</InputLabel>
							<Select
								id="demo-simple-select-outlined"
								name="drinksFoodRating"
								value={placeReview.drinksFoodRating}
								onChange={handleRatingChange}
								label="Food/Drinks"
							>
								<MenuItem value={5}>5</MenuItem>
								<MenuItem value={4}>4</MenuItem>
								<MenuItem value={3}>3</MenuItem>
								<MenuItem value={2}>2</MenuItem>
								<MenuItem value={1}>1</MenuItem>
							</Select>
						</FormControl>
					</Grid>
					<Grid item>
						<FormControl variant="outlined" size="small" className={classes.formControl}>
							<InputLabel id="foodQualitySelect">Service</InputLabel>
							<Select
								id="demo-simple-select-outlined"
								name="serviceRating"
								value={placeReview.serviceRating}
								onChange={handleRatingChange}
								label="Service"
							>
								<MenuItem value={5}>5</MenuItem>
								<MenuItem value={4}>4</MenuItem>
								<MenuItem value={3}>3</MenuItem>
								<MenuItem value={2}>2</MenuItem>
								<MenuItem value={1}>1</MenuItem>
							</Select>
						</FormControl>
					</Grid>
					<Grid item>
						<FormControl variant="outlined" size="small" className={classes.formControl}>
							<InputLabel id="foodQualitySelect">Interior</InputLabel>
							<Select
								id="demo-simple-select-outlined"
								name="interiorRating"
								value={placeReview.interiorRating}
								onChange={handleRatingChange}
								label="Interior"
							>
								<MenuItem value={5}>5</MenuItem>
								<MenuItem value={4}>4</MenuItem>
								<MenuItem value={3}>3</MenuItem>
								<MenuItem value={2}>2</MenuItem>
								<MenuItem value={1}>1</MenuItem>
							</Select>
						</FormControl>
					</Grid>
					<Grid item>
						<Button
							type="submit"
							variant="contained"
							color="primary"
							disabled={reviewValidator()}
						>
							Add review
						</Button>
					</Grid>
				</Grid>
			</Grid>
		</>
	);
};

export default AddReviewForm;
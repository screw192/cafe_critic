import React, {useState} from 'react';

import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {Checkbox, FormControlLabel, Grid, Typography} from "@material-ui/core";

const AddPlaceForm = ({onSubmit, error, loading}) => {
	const [newPlace, setNewPlace] = useState({
		title: "",
		description: "",
		mainPhoto: "",
		agreement: false,
	});

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(newPlace).forEach(key => {
			formData.append(key, newPlace[key]);
		});

		onSubmit(formData);
	};

	const placeInputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;

		setNewPlace(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const placeImageChangeHandler = e => {
		const file = e.target.files[0];

		setNewPlace(prevState => ({
			...prevState,
			mainPhoto: file,
		}));
	};

	const placeCheckboxChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.checked;

		setNewPlace(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return (
		<Grid
			container
			spacing={1}
			direction="column"
			component="form"
			onSubmit={submitFormHandler}
			noValidate
		>
			<FormElement
				required
				label="Title"
				type="text"
				name="title"
				onChange={placeInputChangeHandler}
				value={newPlace.title}
				error={getFieldError("title")}
			/>
			<FormElement
				required
				label="Description"
				type="text"
				name="description"
				onChange={placeInputChangeHandler}
				value={newPlace.description}
				error={getFieldError("description")}
			/>
			<Grid item xs>
				<FileInput
					label="Photo"
					onChange={placeImageChangeHandler}
					error={getFieldError("mainPhoto")}
				/>
			</Grid>
			<Grid container item xs alignItems="center">
				<Grid item>
					<FormControlLabel
						control={
							<Checkbox
								checked={newPlace.agreement}
								onChange={placeCheckboxChangeHandler}
								name="agreement"
								color="primary"
							/>
						}
						label="I understand"
					/>
				</Grid>
				<Grid item xs={5}>
					<Typography variant="body1">
						By submitting this form I'm blah blah blah 359 ultra unity hardcore.
						Drinking "Okhota krepkoe" and then dancing. My dad drinks herb every day 5 times a day.
					</Typography>
				</Grid>
			</Grid>

			<Grid item>
				<ButtonWithProgress
					type="submit"
					variant="contained"
					color="primary"
					loading={loading}
					disabled={loading}
				>
					Add place
				</ButtonWithProgress>
			</Grid>
		</Grid>
	);
};

export default AddPlaceForm;
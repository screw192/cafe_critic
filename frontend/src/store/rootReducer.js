import {combineReducers} from "redux";

import usersSlice from "./slices/usersSlice";
import notifierSlice from "./slices/notifierSlice";
import placesSlice from "./slices/placesSlice";
import reviewSlice from "./slices/reviewSlice";


const rootReducer = combineReducers({
  users: usersSlice.reducer,
  notifier: notifierSlice.reducer,
	places: placesSlice.reducer,
	review: reviewSlice.reducer,
});

export default rootReducer;
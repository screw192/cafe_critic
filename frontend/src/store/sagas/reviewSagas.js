import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
	addReviewRequest,
	addReviewSuccess,
	addReviewFailure,
} from "../actions/reviewActions";
import {addNotification} from "../actions/notifierActions";
import {fetchPlaceDetailsRequest} from "../actions/placesActions";

export function* addReview({payload: data}) {
	try {
		yield axiosApi.post("/reviews", data.reviewData);

		yield put(addReviewSuccess());
		yield put(addNotification({message: "Review successfully added", options: {variant: "success"}}));

		yield put(fetchPlaceDetailsRequest(data.placeId));
	} catch (error) {
		yield put(addReviewFailure(error));
		yield put(addNotification({message: "Failed to add review", options: {variant: "error"}}));
	}
}

const reviewSagas = [
	takeEvery(addReviewRequest, addReview),
];

export default reviewSagas;
import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
	fetchPlacesRequest,
	fetchPlacesSuccess,
	fetchPlacesFailure, fetchPlaceDetailsSuccess, fetchPlaceDetailsFailure, fetchPlaceDetailsRequest, uploadPlaceRequest
} from "../actions/placesActions";

import {addNotification} from "../actions/notifierActions";
import {historyPush} from "../actions/historyActions";

export function* fetchPlaces() {
	try {
		const response = yield axiosApi.get("/places/all");
		yield put(fetchPlacesSuccess(response.data));
	} catch (error) {
		yield put(fetchPlacesFailure(error.response.data));
		yield put(addNotification({message: "Cannot fetch data", options: {variant: "error"}}));
	}
}

export function* fetchPlaceDetails({payload: placeId}) {
	try {
		const response = yield axiosApi.get(`/places/${placeId}`);
		yield put(fetchPlaceDetailsSuccess(response.data));
	} catch (error) {
		yield put(fetchPlaceDetailsFailure(error.response.data));
		yield put(addNotification({message: "Cannot fetch data", options: {variant: "error"}}));
	}
}

export function* uploadPlace({payload: placeData}) {
	try {
		yield axiosApi.post("/places", placeData);

		yield put(historyPush("/"));
		yield put(addNotification({message: "Place successfully added", options: {variant: "success"}}))
	} catch (e) {
		yield put(addNotification({message: "Failed to add new place", options: {variant: "error"}}));
	}
}

const placesSagas = [
	takeEvery(fetchPlacesRequest, fetchPlaces),
	takeEvery(fetchPlaceDetailsRequest, fetchPlaceDetails),
	takeEvery(uploadPlaceRequest, uploadPlace)
];

export default placesSagas;
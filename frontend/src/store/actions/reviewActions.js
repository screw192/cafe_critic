import reviewSlice from "../slices/reviewSlice";

export const {
	addReviewRequest,
	addReviewSuccess,
	addReviewFailure
} =reviewSlice.actions;
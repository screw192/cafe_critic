import placesSlice from "../slices/placesSlice";

export const {
	fetchPlacesRequest,
	fetchPlacesSuccess,
	fetchPlacesFailure,
	fetchPlaceDetailsRequest,
	fetchPlaceDetailsSuccess,
	fetchPlaceDetailsFailure,
	uploadPlaceRequest,
	uploadPlaceSuccess,
	uploadPlaceFailure,
} = placesSlice.actions;
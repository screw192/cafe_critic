import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
	addReviewLoading: false,
	addReviewError: null,
};

const name = "review";

const reviewSlice = createSlice({
	name,
	initialState,
	reducers: {
		addReviewRequest: state => {
			state.addReviewLoading = true;
		},
		addReviewSuccess: state => {
			state.addReviewLoading = false;
			state.addReviewError = null;
		},
		addReviewFailure: (state, {payload: error}) => {
			state.addReviewLoading = false;
			state.addReviewError = error;
		}
	}
});

export default reviewSlice;
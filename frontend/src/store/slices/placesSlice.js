import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
	placesLoading: false,
	placesError: null,
	places: [],
	placeDetailsLoading: false,
	placeDetailsError: null,
	placeDetails: [],
	addPlaceLoading: false,
	addPlaceError: null,
};

const name = "places";

const placesSlice = createSlice({
	name,
	initialState,
	reducers: {
		fetchPlacesRequest: state => {
			state.placesLoading = true;
		},
		fetchPlacesSuccess: (state, {payload: places}) => {
			state.placesLoading = false;
			state.placesError = null;
			state.places = places;
		},
		fetchPlacesFailure: (state, {payload: error}) => {
			state.placesLoading = false;
			state.placesError = error;
		},
		fetchPlaceDetailsRequest: state => {
			state.placeDetailsLoading = true;
		},
		fetchPlaceDetailsSuccess: (state, {payload: placeDetails}) => {
			state.placeDetailsLoading = false;
			state.placeDetailsError = null;
			state.placeDetails = placeDetails;
		},
		fetchPlaceDetailsFailure: (state, {payload: error}) => {
			state.placeDetailsLoading = false;
			state.placeDetailsError = error;
		},
		uploadPlaceRequest: state => {
			state.addPlaceLoading = true;
		},
		uploadPlaceSuccess: state => {
			state.addPlaceLoading = false;
			state.addPlaceError = null;
		},
		uploadPlaceFailure: (state, {payload: error}) => {
			state.addPlaceLoading = false;
			state.addPlaceError = error;
		},
	}
});

export default placesSlice;